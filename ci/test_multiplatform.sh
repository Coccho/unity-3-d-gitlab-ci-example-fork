#!/usr/bin/env bash

set -x

echo "Testing for $BUILD_TARGET, Unit Type: $TESTING_TYPE"

${UNITY_EXECUTABLE:-xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' unity-editor} \
  -projectPath $UNITY_DIR \
  -runTests \
  -testPlatform playmode \
  -buildTarget $BUILD_TARGET \
  -scriptingbackend $SCRIPTING_BACKEND \
  -testResults $UNITY_DIR/$BUILD_TARGET-results.xml \
  -logFile /dev/stdout \
  -batchmode \
  -nographics \
  -debugCodeOptimization

UNITY_EXIT_CODE=$?

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "Run succeeded, some tests failed";
  if [ $TESTING_TYPE == 'JUNIT' ]; then
    echo "Converting results to JUNit for analysis";
    saxonb-xslt -s $UNITY_DIR/$BUILD_TARGET-results.xml -xsl $CI_PROJECT_DIR/ci/nunit-transforms/nunit3-junit.xslt >$UNITY_DIR/$BUILD_TARGET-junit-results.xml
  fi
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
  if [ $TESTING_TYPE == 'JUNIT' ]; then
    echo "Not converting results to JUNit";
  fi
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
  if [ $TESTING_TYPE == 'JUNIT' ]; then
    echo "Not converting results to JUNit";
  fi
fi

cat $UNITY_DIR/$BUILD_TARGET-results.xml | grep test-run | grep Passed
exit $UNITY_EXIT_CODE
